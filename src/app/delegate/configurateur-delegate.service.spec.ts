import { TestBed } from '@angular/core/testing';

import { ConfigurateurDelegateService } from './configurateur-delegate.service';

describe('ConfigurateurDelegateService', () => {
  let service: ConfigurateurDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigurateurDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
