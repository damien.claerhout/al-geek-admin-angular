import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { Stock } from '../interfaces/stock';
import { retry, catchError } from 'rxjs/operators';
import { Approvisionnement } from '../interfaces/approvisionnement';
import { Fournisseur } from '../interfaces/fournisseur';

@Injectable({
  providedIn: 'root'
})
export class ApprovisionnementDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/approvisionnement";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }


  public findByFournisseur(fournisseur: Fournisseur){
    return this.httpClient.post<Approvisionnement[]>(this.URL+"/by-fournisseur", fournisseur)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public findByStock(stock: Stock){
    return this.httpClient.post<Approvisionnement[]>(this.URL+"/by-stock", stock)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public ajouter(approvisionnement: Approvisionnement){
    return this.httpClient.post<Approvisionnement>(this.URL+"/add", approvisionnement)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(approvisionnement: Approvisionnement){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(approvisionnement: Approvisionnement){
    return this.httpClient.put<Approvisionnement>(this.URL, approvisionnement)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<Stock>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<Approvisionnement[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }


}
