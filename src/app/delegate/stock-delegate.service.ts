import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Stock } from '../interfaces/stock';
import { Produit } from '../interfaces/produit';
import { TypeProduit } from '../interfaces/type-produit';

@Injectable({
  providedIn: 'root'
})
export class StockDelegateService {

  // URL qui pointe vers l'API déployée sur AWS
  //private URL = "http://algeek-env.mfkt3xidnq.eu-west-3.elasticbeanstalk.com:8080/projetAL/algeek-api/public/stock";
  private URL = "http://localhost:8080/projetAL/algeek-api/public/stock";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getByTypeProduit(typeProduitId: Number){
    return this.httpClient.get<Stock[]>(this.URL +"/get-by-type?id=" + typeProduitId)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getByQuantiteDispo(quantite: number){
    return this.httpClient.get<Stock[]>(this.URL +"/get-by-quantite?quantite=" + quantite)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getByProduit(produit: Produit){
    return this.httpClient.post<Stock>(this.URL+"/get-by-produit", produit)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public ajouter(stock: Stock){
    return this.httpClient.post<Stock>(this.URL+"/add", stock)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(stock: Stock){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(stock: Stock){
    return this.httpClient.put<Stock>(this.URL, stock)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<Stock>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<Stock[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }

}
