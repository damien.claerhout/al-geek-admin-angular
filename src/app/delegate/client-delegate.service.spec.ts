import { TestBed } from '@angular/core/testing';

import { ClientDelegateService } from './client-delegate.service';

describe('ClientDelegateService', () => {
  let service: ClientDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClientDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
