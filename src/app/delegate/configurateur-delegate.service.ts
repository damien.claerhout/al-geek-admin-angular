import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { TypeProduit } from '../interfaces/type-produit';
import { ComparaisonProduits } from '../interfaces/comparaison-produits';

@Injectable({
  providedIn: 'root'
})
export class ConfigurateurDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/configurateur";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getTypesComposants(){
    return this.httpClient.get<TypeProduit[]>(this.URL+ "/composants")
                          .pipe(retry(3), catchError(this.handleError));
  }

  public estCompatibleAvec(comparaisonProduit: ComparaisonProduits){
    return this.httpClient.get<boolean>(this.URL+ "/compatibilite")
                          .pipe(retry(3), catchError(this.handleError));
  } 


}
