import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { PaiementDiffere } from '../interfaces/paiement-differe';

@Injectable({
  providedIn: 'root'
})
export class PaiementDiffereDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/paiement";
  private URL_MICROSERVICE = "http://localhost:8090/paiement";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public findPaiementByMensualite(somme: number, mois: number){
    return this.httpClient.get<number>(this.URL_MICROSERVICE +"?a="+ somme +"&b="+ mois)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public ajouter(paiementDiffere: PaiementDiffere){
    return this.httpClient.post<PaiementDiffere>(this.URL+"/add", paiementDiffere)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(paiementDiffere: PaiementDiffere){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(paiementDiffere: PaiementDiffere){
    return this.httpClient.put<PaiementDiffere>(this.URL, paiementDiffere)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<PaiementDiffere>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<PaiementDiffere[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }
}
