import { TestBed } from '@angular/core/testing';

import { LocaleDelegateService } from './locale-delegate.service';

describe('LocaleDelegateService', () => {
  let service: LocaleDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocaleDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
