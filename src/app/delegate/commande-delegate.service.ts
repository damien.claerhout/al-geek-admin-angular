import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Commande } from '../interfaces/commande';

@Injectable({
  providedIn: 'root'
})
export class CommandeDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/commande";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }


  public validerCommande(commande: Commande){
    return this.httpClient.post<Commande>(this.URL+"/valider-commande", commande)
                          .pipe(retry(3), catchError(this.handleError));
  } 

  public getCommandesPourClient(idClient : number){
    return this.httpClient.get<Commande[]>(this.URL+ "/client?idClient=" + idClient)
                          .pipe(retry(3), catchError(this.handleError));
  }  

  public getCommandesAtraiter(){
    return this.httpClient.get<Commande[]>(this.URL+"/commandes-a-traiter")
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getCommandesDuJour(date: Date){
    return this.httpClient.post<Commande[]>(this.URL+"/date-du-jour", date)
                          .pipe(retry(3), catchError(this.handleError));
  } 

  public ajouter(commande: Commande){
    return this.httpClient.post<Commande>(this.URL+"/add", commande)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(commande: Commande){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(commande: Commande){
    return this.httpClient.put<Commande>(this.URL, commande)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<Commande>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<Commande[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }

}
