import { TestBed } from '@angular/core/testing';

import { InfosBancairesDelegateService } from './infos-bancaires-delegate.service';

describe('InfosBancairesDelegateService', () => {
  let service: InfosBancairesDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InfosBancairesDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
