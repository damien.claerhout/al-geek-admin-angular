import { TestBed } from '@angular/core/testing';

import { StockDelegateService } from './stock-delegate.service';

describe('StockDelegateService', () => {
  let service: StockDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StockDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
