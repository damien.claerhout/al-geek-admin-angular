import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Produit } from '../interfaces/produit';

@Injectable({
  providedIn: 'root'
})
export class TriProduitDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/tri";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public trierParMarqueCroissant(produits : Produit[]){
    return this.httpClient.post<Produit[]>(this.URL+ "/marque-croissant", produits)
                          .pipe(retry(3), catchError(this.handleError));
  }  

  public trierParMarqueDecroissant(produits : Produit[]){
    return this.httpClient.post<Produit[]>(this.URL+ "/marque-decroissant", produits)
                          .pipe(retry(3), catchError(this.handleError));
  }  

  public trierParPrixCroissant(produits : Produit[]){
    return this.httpClient.post<Produit[]>(this.URL+ "/prix-croissant", produits)
                          .pipe(retry(3), catchError(this.handleError));
  }  

  public trierParPrixDecroissant(produits : Produit[]){
    return this.httpClient.post<Produit[]>(this.URL+ "/prix-decroissant", produits)
                          .pipe(retry(3), catchError(this.handleError));
  }  

  public trierParNomAlphabetique(produits : Produit[]){
    return this.httpClient.post<Produit[]>(this.URL+ "/alphabetique", produits)
                          .pipe(retry(3), catchError(this.handleError));
  }  
}
