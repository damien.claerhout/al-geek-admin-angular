import { TestBed } from '@angular/core/testing';

import { DepannageDelegateService } from './depannage-delegate.service';

describe('DepannageDelegateService', () => {
  let service: DepannageDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DepannageDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
