import { TestBed } from '@angular/core/testing';

import { CommandeDelegateService } from './commande-delegate.service';

describe('CommandeDelegateService', () => {
  let service: CommandeDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommandeDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
