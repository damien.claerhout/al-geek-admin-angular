import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { ProduitPanier } from '../interfaces/produit-panier';
import { Panier } from '../interfaces/panier';

@Injectable({
  providedIn: 'root'
})
export class PanierDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/panier";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public ajoutPanier(produitPanier: ProduitPanier){
    return this.httpClient.post<Panier>(this.URL+"/ajout", produitPanier)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public retirerProduitDuPanier(produitPanier: ProduitPanier){
    return this.httpClient.post<Panier>(this.URL+"/retrait", produitPanier)
                          .pipe(retry(3), catchError(this.handleError));
  }
}
