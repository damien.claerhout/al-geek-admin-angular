import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Categorie } from '../interfaces/categorie';

@Injectable({
  providedIn: 'root'
})
export class CategorieDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/categorie";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getRootCategorie(){
    return this.httpClient.get<Categorie>(this.URL + "/root")
                          .pipe(retry(3), catchError(this.handleError));
  }

  public ajouter(categorie: Categorie){
    return this.httpClient.post<Categorie>(this.URL+"/add", categorie)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(categorie: Categorie){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(categorie: Categorie){
    return this.httpClient.put<Categorie>(this.URL, categorie)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<Categorie>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<Categorie[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }


}
