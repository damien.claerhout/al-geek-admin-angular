import { TestBed } from '@angular/core/testing';

import { CategorieDelegateService } from './categorie-delegate.service';

describe('CategorieDelegateService', () => {
  let service: CategorieDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CategorieDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
