import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { Fournisseur } from '../interfaces/fournisseur';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FournisseurDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/fournisseur";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public ajouter(fournisseur: Fournisseur){
    return this.httpClient.post<Fournisseur>(this.URL+"/add", fournisseur)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(fournisseur: Fournisseur){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(fournisseur: Fournisseur){
    return this.httpClient.put<Fournisseur>(this.URL, fournisseur)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<Fournisseur>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<Fournisseur[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }
}
