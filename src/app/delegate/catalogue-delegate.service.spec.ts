import { TestBed } from '@angular/core/testing';

import { CatalogueDelegateService } from './catalogue-delegate.service';

describe('CatalogueDelegateService', () => {
  let service: CatalogueDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatalogueDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
