import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { LigneCommande } from '../interfaces/ligne-commande';
import { Commande } from '../interfaces/commande';

@Injectable({
  providedIn: 'root'
})
export class LigneCommandeDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/ligne-commande";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getByCommande(id: number){
    return this.httpClient.get<LigneCommande[]>(this.URL+"/get-by-commande?commandeId="+id)
                          .pipe(retry(3), catchError(this.handleError));
  } 

  public ajouter(ligneCommande: LigneCommande){
    return this.httpClient.post<LigneCommande>(this.URL+"/add", ligneCommande)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(ligneCommande: LigneCommande){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(ligneCommande: LigneCommande){
    return this.httpClient.put<LigneCommande>(this.URL, ligneCommande)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<LigneCommande>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<LigneCommande[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }

}
