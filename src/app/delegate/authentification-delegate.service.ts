import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { CredentialsInit } from '../interfaces/credentials-init';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/authentification";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public initializeCredentials(credentialsInit: CredentialsInit){
    return this.httpClient.post<CredentialsInit>(this.URL+ "/init", credentialsInit)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public authentificate(credentialsInit: CredentialsInit){
    return this.httpClient.post<CredentialsInit>(this.URL, credentialsInit)
                          .pipe(retry(3), catchError(this.handleError));
  }
}
