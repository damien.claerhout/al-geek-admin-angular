import { TestBed } from '@angular/core/testing';

import { AuthentificationDelegateService } from './authentification-delegate.service';

describe('AuthentificationDelegateService', () => {
  let service: AuthentificationDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthentificationDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
