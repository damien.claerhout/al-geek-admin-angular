import { TestBed } from '@angular/core/testing';

import { FournisseurDelegateService } from './fournisseur-delegate.service';

describe('FournisseurDelegateService', () => {
  let service: FournisseurDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FournisseurDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
