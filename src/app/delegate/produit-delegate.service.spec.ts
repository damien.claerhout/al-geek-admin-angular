import { TestBed } from '@angular/core/testing';

import { ProduitDelegateService } from './produit-delegate.service';

describe('ProduitDelegateService', () => {
  let service: ProduitDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProduitDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
