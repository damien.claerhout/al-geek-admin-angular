import { TestBed } from '@angular/core/testing';

import { LigneCommandeDelegateService } from './ligne-commande-delegate.service';

describe('LigneCommandeDelegateService', () => {
  let service: LigneCommandeDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LigneCommandeDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
