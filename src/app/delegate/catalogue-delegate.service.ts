import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Produit } from '../interfaces/produit';

@Injectable({
  providedIn: 'root'
})
export class CatalogueDelegateService {

  // URL qui pointe vers l'API déployée sur AWS
  //private URL = "http://algeek-env.mfkt3xidnq.eu-west-3.elasticbeanstalk.com:8080/projetAL/algeek-api/public/catalogue";
  private URL = "http://localhost:8080/projetAL/algeek-api/public/catalogue";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }


  public getMeilleuresVentes(numResultatsMax : number){
    return this.httpClient.get<Produit[]>(this.URL+"/meilleuresVentes?numResultatsMax="+ numResultatsMax)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getNouveautes(){
    return this.httpClient.get<Produit[]>(this.URL+"/nouveautes")
                          .pipe(retry(3), catchError(this.handleError));
  }

  public ajouter(produit: Produit){
    return this.httpClient.post<Produit>(this.URL+"/add", produit)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(produit: Produit){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(produit: Produit){
    return this.httpClient.put<Produit>(this.URL, produit)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<Produit>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<Produit[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }

}
