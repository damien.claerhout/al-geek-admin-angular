import { TestBed } from '@angular/core/testing';

import { PanierDelegateService } from './panier-delegate.service';

describe('PanierDelegateService', () => {
  let service: PanierDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PanierDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
