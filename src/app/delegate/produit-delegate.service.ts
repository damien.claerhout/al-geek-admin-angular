import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Produit } from '../interfaces/produit';
import { TypeProduit } from '../interfaces/type-produit';

@Injectable({
  providedIn: 'root'
})
export class ProduitDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/produit";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getTousTypesProduits(){
    return this.httpClient.get<TypeProduit[]>(this.URL+ "/all-types")
                          .pipe(retry(3), catchError(this.handleError));
  }  

  public getProduitAvecCaracteristiques(id : number){
    return this.httpClient.get<Produit>(this.URL+ "/caracteristique?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  } 

  public getProduitsParType(id : number, chargerCaracteristiques : boolean){
    return this.httpClient.get<Produit[]>(this.URL+ "/type?id=" + id +"&chargerCaracteristiques=" + chargerCaracteristiques)
                          .pipe(retry(3), catchError(this.handleError));
  } 

  public filtrerUsageOccasionnel(listP : Produit[], typeOrdi : string){
    return this.httpClient.post<Produit[]>(this.URL+ "/filtrer-usage-occasionnel?typeOrdi=" + typeOrdi, listP)
                          .pipe(retry(3), catchError(this.handleError));
  }  

  public filtrerUsageRegulier(listP : Produit[], typeOrdi : string){
    return this.httpClient.post<Produit[]>(this.URL+ "/filtrer-usage-regulier?typeOrdi=" + typeOrdi, listP)
                          .pipe(retry(3), catchError(this.handleError));
  }  

  public filtrerUsageIntensif(listP : Produit[], typeOrdi : string){
    return this.httpClient.post<Produit[]>(this.URL+ "/filtrer-usage-intensif?typeOrdi=" + typeOrdi, listP)
                          .pipe(retry(3), catchError(this.handleError));
  }  

  public trierMoinsCherAuPlusCher(idType : number){
    return this.httpClient.get<Produit[]>(this.URL+"/tri-du-plus-cher-au-moins-cher?idType=" + idType)
                          .pipe(retry(3), catchError(this.handleError));
  } 

  public ajouter(produit: Produit){
    return this.httpClient.post<Produit>(this.URL+"/add", produit)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(produit: Produit){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(produit: Produit){
    return this.httpClient.put<Produit>(this.URL, produit)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<Produit>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<Produit[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }
}
