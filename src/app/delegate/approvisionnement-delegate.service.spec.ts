import { TestBed } from '@angular/core/testing';

import { ApprovisionnementDelegateService } from './approvisionnement-delegate.service';

describe('ApprovisionnementDelegateService', () => {
  let service: ApprovisionnementDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApprovisionnementDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
