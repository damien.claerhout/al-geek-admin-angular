import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { InfosBancaires } from '../interfaces/infos-bancaires';

@Injectable({
  providedIn: 'root'
})
export class InfosBancairesDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/infos-bancaires";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public ajouter(infosBancaires: InfosBancaires){
    return this.httpClient.post<InfosBancaires>(this.URL+"/add", infosBancaires)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(infosBancaires: InfosBancaires){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(infosBancaires: InfosBancaires){
    return this.httpClient.put<InfosBancaires>(this.URL, infosBancaires)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<InfosBancaires>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<InfosBancaires[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }

}
