import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { throwError, Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Depanneur } from '../interfaces/depanneur';

@Injectable({
  providedIn: 'root'
})
export class DepannageDelegateService {

  // private URL = "http://35.181.48.243:8080/algeekrep/";

  private URL = "http://localhost:8180/depanneur-api/";

  depanneurs: Depanneur[] = [];

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }


  public getAll() {
    return this.httpClient.get<Depanneur[]>(this.URL)
      .pipe(retry(1), catchError(this.handleError));
  }


  public getDepanneurById(id: number) {
    const dep = this.depanneurs.find(
      (depanneurObject) => {
        return depanneurObject.id === id;
      }
    );
    return dep;
  }


  public ajouter(depanneur: Depanneur) {
    return this.httpClient.post<Depanneur>(this.URL + "add", depanneur)
      .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(depanneur: Depanneur) {
    return this.httpClient.put<Depanneur>(this.URL + "modif", depanneur)
      .pipe(retry(3), catchError(this.handleError));
  }

}
