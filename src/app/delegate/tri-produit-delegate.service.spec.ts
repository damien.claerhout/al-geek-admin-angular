import { TestBed } from '@angular/core/testing';

import { TriProduitDelegateService } from './tri-produit-delegate.service';

describe('TriProduitDelegateService', () => {
  let service: TriProduitDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TriProduitDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
