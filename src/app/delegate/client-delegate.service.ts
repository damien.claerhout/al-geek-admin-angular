import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Client } from '../interfaces/client';
import { LoginRequest } from '../interfaces/login-request';

@Injectable({
  providedIn: 'root'
})
export class ClientDelegateService {

  private URL = "http://localhost:8080/projetAL/algeek-api/public/client";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public doConnecter(loginRequest: LoginRequest){
    return this.httpClient.post<Client>(this.URL+"/login", loginRequest)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public findByEmail(email: string){
    return this.httpClient.post<Client>(this.URL+"/get-email", email)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public ajouter(client: Client){
    return this.httpClient.post<Client>(this.URL+"/add", client)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(client: Client){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(client: Client){
    return this.httpClient.put<Client>(this.URL, client)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<Client>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<Client[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }


}
