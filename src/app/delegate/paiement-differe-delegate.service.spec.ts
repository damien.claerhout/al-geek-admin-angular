import { TestBed } from '@angular/core/testing';

import { PaiementDiffereDelegateService } from './paiement-differe-delegate.service';

describe('PaiementDiffereDelegateService', () => {
  let service: PaiementDiffereDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaiementDiffereDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
