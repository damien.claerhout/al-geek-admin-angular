import { TestBed } from '@angular/core/testing';

import { VilleDelegateService } from './ville-delegate.service';

describe('VilleDelegateService', () => {
  let service: VilleDelegateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VilleDelegateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
