import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { Ville } from '../interfaces/ville';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VilleDelegateService {

 
  private URL = "http://localhost:8080/projetAL/algeek-api/public/ville";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }


  public ajouter(ville: Ville){
    return this.httpClient.post<Ville>(this.URL+"/add", ville)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public supprimer(ville: Ville){
    return this.httpClient.delete<boolean>(this.URL)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public modifier(ville: Ville){
    return this.httpClient.put<Ville>(this.URL, ville)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public rechercherParId(id: number){
    return this.httpClient.get<Ville>(this.URL + "?id=" + id)
                          .pipe(retry(3), catchError(this.handleError));
  }

  public getAll(){
    return this.httpClient.get<Ville[]>(this.URL+"/all")
                          .pipe(retry(3), catchError(this.handleError));
  }
}
