import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatIconModule} from '@angular/material/icon';
import { MatCardModule} from '@angular/material/card';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule} from '@angular/material/core';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule} from '@angular/material/input';
import { MatButtonModule} from '@angular/material/button';
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { MoteurRechercheClientComponent } from './components/moteur-recherche-client/moteur-recherche-client.component';
import { FicheClientComponent } from './components/fiche-client/fiche-client.component';
import { MoteurRechercheFournisseurComponent } from './components/moteur-recherche-fournisseur/moteur-recherche-fournisseur.component';
import { FicheFournisseurComponent } from './components/fiche-fournisseur/fiche-fournisseur.component';
import { AppHeaderComponent } from './components/app-header/app-header.component';
import { AppFooterComponent } from './components/app-footer/app-footer.component';
import { FicheProduitComponent } from './components/fiche-produit/fiche-produit.component';
import { MoteurRechercheDepanneurComponent } from './components/moteur-recherche-depanneur/moteur-recherche-depanneur.component';
import { FicheDepanneurComponent } from './components/fiche-depanneur/fiche-depanneur.component';
import { UpdateFicheClientComponent } from './components/update-fiche-client/update-fiche-client.component';
import { StocksComponent } from './components/stocks/stocks.component';
import { UpdateFicheDepanneurComponent } from './components/update-fiche-depanneur/update-fiche-depanneur.component';
import { AddDepanneurComponent } from './components/add-depanneur/add-depanneur.component';
import { UpdateFournisseurComponent } from './components/update-fournisseur/update-fournisseur.component';
import { AfficherCommandeComponent } from './components/afficher-commande/afficher-commande.component';
import { HomeAdminComponent } from './components/home-admin/home-admin.component';
import { UpdateFicheProduitComponent } from './components/update-fiche-produit/update-fiche-produit.component';
import { ApprovisionnementsComponent } from './components/approvisionnements/approvisionnements.component';
import { AddFournisseurComponent } from './components/add-fournisseur/add-fournisseur.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { ListCommandesPendingComponent } from './components/list-commandes-pending/list-commandes-pending.component';
import { AfficherApprovisionnementComponent } from './components/afficher-approvisionnement/afficher-approvisionnement.component';
import { StocksRuptureComponent } from './components/stocks-rupture/stocks-rupture.component';
import { ListCommandesDuJourComponent } from './components/list-commandes-du-jour/list-commandes-du-jour.component';
import { KibanaComponent } from './components/kibana/kibana.component';
registerLocaleData(localeFr, 'fr-FR');


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    CatalogueComponent,
    MoteurRechercheClientComponent,
    FicheClientComponent,
    MoteurRechercheFournisseurComponent,
    FicheFournisseurComponent,
    AppHeaderComponent,
    AppFooterComponent,
    FicheProduitComponent,
    MoteurRechercheDepanneurComponent,
    FicheDepanneurComponent,
    UpdateFicheClientComponent,
    StocksComponent,
    UpdateFicheDepanneurComponent,
    AddDepanneurComponent,
    UpdateFournisseurComponent,
    AfficherCommandeComponent,
    HomeAdminComponent,
    UpdateFicheProduitComponent,
    ApprovisionnementsComponent,
    AddFournisseurComponent,
    ListCommandesPendingComponent,
    StocksRuptureComponent,
    ListCommandesDuJourComponent,
    AfficherApprovisionnementComponent,
    StocksRuptureComponent,
    KibanaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatProgressSpinnerModule,
    NgbModule,
    FormsModule,
    DataTablesModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule

  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
