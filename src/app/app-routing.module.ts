
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { MoteurRechercheClientComponent } from './components/moteur-recherche-client/moteur-recherche-client.component';
import { FicheClientComponent } from './components/fiche-client/fiche-client.component';
import { UpdateFicheClientComponent } from './components/update-fiche-client/update-fiche-client.component';
import { MoteurRechercheFournisseurComponent } from './components/moteur-recherche-fournisseur/moteur-recherche-fournisseur.component';
import { FicheFournisseurComponent } from './components/fiche-fournisseur/fiche-fournisseur.component';
import { UpdateFournisseurComponent } from './components/update-fournisseur/update-fournisseur.component';
import { FicheProduitComponent } from './components/fiche-produit/fiche-produit.component';
import { MoteurRechercheDepanneurComponent } from './components/moteur-recherche-depanneur/moteur-recherche-depanneur.component';
import { FicheDepanneurComponent } from './components/fiche-depanneur/fiche-depanneur.component';
import { UpdateFicheDepanneurComponent } from './components/update-fiche-depanneur/update-fiche-depanneur.component';
import { AddDepanneurComponent } from './components/add-depanneur/add-depanneur.component';
import { StocksComponent } from './components/stocks/stocks.component';
import { AfficherCommandeComponent } from './components/afficher-commande/afficher-commande.component';
import { HomeAdminComponent } from './components/home-admin/home-admin.component';
import { UpdateFicheProduitComponent } from './components/update-fiche-produit/update-fiche-produit.component';
import { ApprovisionnementsComponent } from './components/approvisionnements/approvisionnements.component';
import { AddFournisseurComponent } from './components/add-fournisseur/add-fournisseur.component';
import { ListCommandesPendingComponent } from './components/list-commandes-pending/list-commandes-pending.component';
import { AfficherApprovisionnementComponent } from './components/afficher-approvisionnement/afficher-approvisionnement.component';
import { StocksRuptureComponent } from './components/stocks-rupture/stocks-rupture.component';
import { ListCommandesDuJourComponent } from './components/list-commandes-du-jour/list-commandes-du-jour.component';
import { KibanaComponent } from './components/kibana/kibana.component';


const routes: Routes = [
  { path:'', redirectTo: 'home', pathMatch: 'full'},
  { path: 'afficherApprovisionnement/:id', component : AfficherApprovisionnementComponent },
  { path: 'afficherCommande/:id', component : AfficherCommandeComponent },
  { path: 'approvisionnements', component: ApprovisionnementsComponent },
  { path: 'catalogue', component: CatalogueComponent },
  { path: 'ficheClient/:id', component: FicheClientComponent },
  { path: 'ficheDepanneur/:id', component: FicheDepanneurComponent },
  { path: 'ficheFournisseur/:id', component: FicheFournisseurComponent },
  { path: 'ficheProduit/:id', component: FicheProduitComponent },
  { path: 'home', component: HomeAdminComponent },
  { path: 'kibana', component : KibanaComponent },
  { path: 'listecommandesatraiter', component : ListCommandesPendingComponent },
  { path: 'listecommandesdujour', component : ListCommandesDuJourComponent },
  { path: 'moteurClient', component: MoteurRechercheClientComponent },
  { path: 'moteurDepanneur', component: MoteurRechercheDepanneurComponent },
  { path: 'moteurFournisseur', component: MoteurRechercheFournisseurComponent },
  { path: 'newDepanneur', component : AddDepanneurComponent },
  { path: 'newFournisseur', component : AddFournisseurComponent },
  { path: 'stocks', component: StocksComponent },
  { path: 'stockslimites', component : StocksRuptureComponent },
  { path: 'updateFicheClient/:id', component: UpdateFicheClientComponent },
  { path: 'updateFournisseur/:id', component : UpdateFournisseurComponent },
  { path: 'updateFicheProduit/:id', component: UpdateFicheProduitComponent },
  { path: 'updateFicheDepanneur/:id', component : UpdateFicheDepanneurComponent },

];

@NgModule({
   imports: [RouterModule.forRoot(routes /*,{onSameUrlNavigation: 'reload'}*/)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
