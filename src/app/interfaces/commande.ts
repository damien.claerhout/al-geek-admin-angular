import { LigneCommande } from './ligne-commande';
import { Client } from './client';
import { InfosBancaires } from './infos-bancaires';



export interface Commande {
    id: number;
    listLigneCommande: LigneCommande[];
    dateCreation: Date;
    dateExpedition: Date;
    client: Client;
    infosBank: InfosBancaires;
    prix: number;
}
