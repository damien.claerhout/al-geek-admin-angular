import { TypeProduit } from './type-produit';


export interface Categorie {
    id: number;
    parent: Categorie;
    enfants: Categorie[];
    nomAffiche: string;
    typeProduit: TypeProduit;
}
