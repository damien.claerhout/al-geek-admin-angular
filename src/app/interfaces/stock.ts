import { Produit } from './produit';

export interface Stock {
    id: number;
    produit: Produit;
    quantiteDispoSiteInternet: number;
    quantiteDispoPhysique: number;
}
