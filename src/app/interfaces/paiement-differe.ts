import { Client } from './client';
import { Commande } from './commande';

export interface PaiementDiffere {
    id: number;
    somme: number;
    mois: number;
    mensualite: number;
    client: Client;
    commande: Commande;
}
