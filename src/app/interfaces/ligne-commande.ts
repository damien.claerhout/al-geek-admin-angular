
import { Commande } from './commande';
import { Produit } from './produit';

export interface LigneCommande {
    id: number;
    quantite: number;
    prixTotal: number;
    produit: Produit;
    commande: Commande;
}
