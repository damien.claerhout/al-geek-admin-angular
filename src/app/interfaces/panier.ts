import { Client } from './client';
import { LigneCommande } from './ligne-commande';

export interface Panier {
    client: Client;
    listLC: LigneCommande[];
}
