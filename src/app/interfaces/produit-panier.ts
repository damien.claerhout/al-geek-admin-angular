import { Panier } from './panier';
import { Produit } from './produit';

export interface ProduitPanier {
    panier : Panier;
    produit: Produit;
}
