export interface Client {
    id: number;
    nom: string;
    prenom: string;
    email: string;
    hashedPassword: string;
    salt: string;
    adresse: string;
    codePostal: string;
    ville: string;
    numeroFixe: string;
    numeroPort: string;
}
