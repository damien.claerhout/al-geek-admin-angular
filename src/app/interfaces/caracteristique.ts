import { Produit } from './produit';

export interface Caracteristique {
    id: number;
    cle: string;
    unite: string;
    type: string;
    valeurString: string;
    valeurDouble: string;
    valeurInteger: string;
    valeurBoolean: string;
    produit: Produit;
}
