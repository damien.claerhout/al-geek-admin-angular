export interface Ville {
    id: number;
    nom: string;
    codePostal: string;
}

