import { Produit } from './produit';
import { Stock } from './stock';
import { Fournisseur } from './fournisseur';

export class Approvisionnement {
    id: number;
    fournisseur: Fournisseur;
    quantiteCommandee: number;
    prix: number;
    dateApprovisionnement: Date;
    stock: Stock;

}
