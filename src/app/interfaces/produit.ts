import { TypeProduit } from './type-produit';
import { Marque } from './marque';
import { Caracteristique } from './caracteristique';

export class Produit {
    id: number;
    nom: string;
    description: string;
    photo: string;
    prix: number;
    type: TypeProduit;
    marque: Marque;
    caracteristiques: Caracteristique[];
    quantite: number;

}
