export interface InfosBancaires {
    id: number;
    typeCarte: string;
    numCarte: string;
    dateCarte: Date;
    cryptogramme: string;
}
