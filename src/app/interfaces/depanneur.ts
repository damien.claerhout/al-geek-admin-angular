export class Depanneur {
    id : number;
    nomSociete : string;
    adresse : string;
    ville : string;
    lat: number;
    lon : number;
    telephone : string;
    email : string;
}
