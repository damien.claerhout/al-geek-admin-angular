export interface Credentials {
    login : string;
    salt :  string;
    hashedPassword: string;
}
