import { Produit } from './produit';


export interface ComparaisonProduits {
    premierProduit: Produit;
    secondProduit: Produit;
}
