import { Ville } from './ville';
import { Approvisionnement } from './approvisionnement';

export class Fournisseur {
    id: number;
    nom: string;
    adresse: string;
    complementAdresse: string;
    ville: Ville;
    numeroFixe: string;
    numeroPort: string;
    email: string;
}


