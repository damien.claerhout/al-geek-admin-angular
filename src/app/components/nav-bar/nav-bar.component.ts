import { Component, OnInit } from '@angular/core';
import { DepannageDelegateService } from 'src/app/delegate/depannage-delegate.service';
import { HttpClient } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Depanneur } from 'src/app/interfaces/depanneur';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  isOK = true;
  depanneurs: Depanneur[] = [];
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private depanneurDelegate: DepannageDelegateService) { }

  ngOnInit(): void {
    this.appelWebServiceDepanneurs();
  }

  appelWebServiceDepanneurs() {
    this.depanneurDelegate.getAll().pipe(takeUntil(this.destroy$))
      .subscribe((res: Depanneur[]) => {
        if (res === null) {
          this.isOK = false;
        }
      }, (err) => {
        this.isOK = false;
        console.log(' *** PASSAGE DANS ERROR ***', err);
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
