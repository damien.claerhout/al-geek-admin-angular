import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { ClientDelegateService } from 'src/app/delegate/client-delegate.service';
import { Client } from 'src/app/interfaces/client';

@Component({
  selector: 'app-update-fiche-client',
  templateUrl: './update-fiche-client.component.html',
  styleUrls: ['./update-fiche-client.component.css']
})
export class UpdateFicheClientComponent implements OnInit, OnDestroy  {

  selectedClient: Client;
  client_id: string;
  _destroyed$: Subject<boolean> = new Subject<boolean>();


  constructor(private clientDelegateService :ClientDelegateService, private route: ActivatedRoute, private router: Router ) {}

  onSubmit() {
    this.updateClient();
  }

  updateClient() {

    const promise =this.clientDelegateService.modifier(this.selectedClient).pipe(takeUntil(this._destroyed$)).toPromise();
    promise.then((res: Client)=>{
        console.log(res);
        this.router.navigate(['/ficheClient/'+this.selectedClient.id]);
    }, (err) => {
      console.log('-----> err', err);
    });
   }

  ngOnInit(): void {
    this.route.paramMap.pipe(takeUntil(this._destroyed$)).subscribe(params => {
      this.client_id = params.get('id');
      console.log("client_id: " + this.client_id);

      
      this.clientDelegateService.rechercherParId(parseInt(this.client_id)).pipe(takeUntil(this._destroyed$)).subscribe((res: Client) =>{
        console.log(res);
        this.selectedClient = res;
  
      }, (err) => {
        console.log('-----> err', err);
      });
    });
 
  }                                                            
  
  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();
  }

}
