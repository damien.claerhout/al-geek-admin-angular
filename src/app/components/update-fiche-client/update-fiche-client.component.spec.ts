import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFicheClientComponent } from './update-fiche-client.component';

describe('UpdateFicheClientComponent', () => {
  let component: UpdateFicheClientComponent;
  let fixture: ComponentFixture<UpdateFicheClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFicheClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFicheClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
