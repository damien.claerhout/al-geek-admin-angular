import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheDepanneurComponent } from './fiche-depanneur.component';

describe('FicheDepanneurComponent', () => {
  let component: FicheDepanneurComponent;
  let fixture: ComponentFixture<FicheDepanneurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheDepanneurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheDepanneurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
