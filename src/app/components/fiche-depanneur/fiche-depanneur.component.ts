import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Depanneur } from 'src/app/interfaces/depanneur';
import { DepannageDelegateService } from 'src/app/delegate/depannage-delegate.service';

@Component({
  selector: 'app-fiche-depanneur',
  templateUrl: './fiche-depanneur.component.html',
  styleUrls: ['./fiche-depanneur.component.css']
})
export class FicheDepanneurComponent implements OnInit, OnDestroy {


  selectedDepanneur: Depanneur;
  depanneur_id: string;
  _destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor( private route: ActivatedRoute, private depannageDelegateService : DepannageDelegateService, private router: Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.selectedDepanneur = this.depannageDelegateService.getDepanneurById(+id);
  }

  updateFicheDepanneur(): void {
    this.router.navigate(['updateFicheDepanneur/' + this.selectedDepanneur.id])
  }

  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();

  }

}
