import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FournisseurDelegateService } from 'src/app/delegate/fournisseur-delegate.service';
import { Router } from '@angular/router';
import { ProduitDelegateService } from 'src/app/delegate/produit-delegate.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Produit } from 'src/app/interfaces/produit';
import { Fournisseur } from 'src/app/interfaces/fournisseur';
import { ApprovisionnementDelegateService } from 'src/app/delegate/approvisionnement-delegate.service';
import { Approvisionnement } from 'src/app/interfaces/approvisionnement';
import { Stock } from 'src/app/interfaces/stock';
import { StockDelegateService } from 'src/app/delegate/stock-delegate.service';

@Component({
  selector: 'app-approvisionnements',
  templateUrl: './approvisionnements.component.html',
  styleUrls: ['./approvisionnements.component.css']
})
export class ApprovisionnementsComponent implements OnInit, OnDestroy {

  _destroyed$: Subject<boolean> = new Subject<boolean>();
  stocks: Stock[] = [];
  fournisseurs: Fournisseur[] = [];
  approvisionnement: Approvisionnement = new Approvisionnement();
  submitted = false;
  stock: Stock;

  constructor(private fournisseurDelegateService: FournisseurDelegateService,
    private approvisionnementDelegateService: ApprovisionnementDelegateService,
    private stockDelegateService: StockDelegateService,
    private router: Router) { }

    
  ajouter() {

    this.approvisionnement.stock.quantiteDispoSiteInternet = (+this.approvisionnement.stock.quantiteDispoSiteInternet + +this.approvisionnement.quantiteCommandee);
    this.approvisionnement.stock.quantiteDispoPhysique = (+this.approvisionnement.stock.quantiteDispoPhysique + +this.approvisionnement.quantiteCommandee);

    this.approvisionnementDelegateService.ajouter(this.approvisionnement).pipe(takeUntil(this._destroyed$)).subscribe((res: Approvisionnement) => {
      this.approvisionnement = res;
    }, (err) => {
      console.log(' *** erreur ***', err);
    });

    const promise =this.stockDelegateService.modifier(this.approvisionnement.stock).pipe(takeUntil(this._destroyed$)).toPromise();
    promise.then((res: Stock)=>{
        console.log(res);
        this.router.navigate(['/stocks']);
    }, (err) => {
      console.log('-----> err', err);
    });
  }


  onSubmit() {
    this.submitted = true;
    this.ajouter();
  }


  ngOnInit(): void {

    this.fournisseurDelegateService.getAll().pipe(takeUntil(this._destroyed$))
                                            .subscribe((res: Fournisseur[]) => {
                                              this.fournisseurs = res;
                                              console.log(res);
                                            }, (err) => {
                                              console.log(' *** erreur ***', err);
      });

    this.stockDelegateService.getAll().pipe(takeUntil(this._destroyed$))
                                      .subscribe((res: Stock[]) => {
                                        this.stocks = res;
                                        console.log(res);
                                      }, (err) => {
                                        console.log(' *** erreur ***', err);
                                      });
  }


  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();
  }

}
