import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Fournisseur } from 'src/app/interfaces/fournisseur';
import { FournisseurDelegateService } from 'src/app/delegate/fournisseur-delegate.service';
import { Ville } from 'src/app/interfaces/ville';
import { VilleDelegateService } from 'src/app/delegate/ville-delegate.service';

@Component({
  selector: 'app-update-fournisseur',
  templateUrl: './update-fournisseur.component.html',
  styleUrls: ['./update-fournisseur.component.css']
})
export class UpdateFournisseurComponent implements OnInit, OnDestroy {

  selectedFournisseur: Fournisseur;
  fournisseur_id: string;
  _destroyed$: Subject<boolean> = new Subject<boolean>();
  villes : Ville[] = [];


  constructor(private fournisseurDelegateService :FournisseurDelegateService,private villeDelegateService: VilleDelegateService, private route: ActivatedRoute, private router: Router ) {}

   updateFournisseur() {

    const promise =this.fournisseurDelegateService.modifier(this.selectedFournisseur).pipe(takeUntil(this._destroyed$)).toPromise();
    promise.then((res: Fournisseur)=>{
        console.log(res);
        this.router.navigate(['/ficheFournisseur/'+this.selectedFournisseur.id]);
    }, (err) => {
      console.log('-----> err', err);
    });


   }

   onSubmit() {
    this.updateFournisseur();
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(takeUntil(this._destroyed$)).subscribe(params => {
      
      this.fournisseur_id = params.get('id');
 
      this.fournisseurDelegateService.rechercherParId(parseInt(this.fournisseur_id)).pipe(takeUntil(this._destroyed$))
                                                                                    .subscribe((res: Fournisseur) =>{
                                                                                        console.log(res);
                                                                                        this.selectedFournisseur = res;
                                                                                  
                                                                                    }, (err) => {
                                                                                        console.log('-----> err', err);
                                                                                    });
    });

    this.villeDelegateService.getAll().pipe(takeUntil(this._destroyed$))
                                      .subscribe((res: Ville[]) =>{
                                          console.log(res);
                                          this.villes = res;
    });
 
  }                                                            
  
  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();

  }

}
