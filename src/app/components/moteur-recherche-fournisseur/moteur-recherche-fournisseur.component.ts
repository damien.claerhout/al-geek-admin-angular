import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Fournisseur } from 'src/app/interfaces/fournisseur';
import { FournisseurDelegateService } from 'src/app/delegate/fournisseur-delegate.service';

@Component({
  selector: 'app-moteur-recherche-fournisseur',
  templateUrl: './moteur-recherche-fournisseur.component.html',
  styleUrls: ['./moteur-recherche-fournisseur.component.css']
})
export class MoteurRechercheFournisseurComponent implements OnInit, OnDestroy {

  fournisseurs: Fournisseur[] = [];

  cols: any[];

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  _destroyed$: Subject<boolean> = new Subject<boolean>();


  constructor(private router: Router, private clientDelegateService: FournisseurDelegateService) { }


  someClickHandler(info: any): void {
    console.log(info[0]);
    this.router.navigate(['/ficheFournisseur/'+ info[0]]);
  }

  ngOnInit(): void {

    this.clientDelegateService.getAll().pipe(takeUntil(this._destroyed$))
                                        .subscribe((res: Fournisseur[]) =>{
                                          console.log(res);
                                          this.fournisseurs = res;

                                          this.dtTrigger.next();
                                        }, (err) => {
                                          console.log('-----> err', err);
    });


    this.dtOptions = {
      order: [ 1, 'asc' ],
      language: {
        processing:     "Traitement...",
        emptyTable:     "Aucune donnée disponible dans le tableau",
        info:           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        infoEmpty:      "Affichage de l'élément 0 à 0 sur 0 élément",
        infoFiltered:   "(filtré à partir de _MAX_ éléments au total)",
        infoPostFix:    "",
        lengthMenu:     "Afficher _MENU_ éléments",
        loadingRecords: "Chargement...",
        search:         "Rechercher :",
        zeroRecords:    "Aucun élément correspondant trouvé",
        paginate: {
            first:    "Premier",
            last:     "Dernier",
            next:     "Suivant",
            previous: "Précédent"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
      },
      responsive: true,

      rowCallback: (row: Node, id: any[] | Object, index: number) => {
        const self = this;
        // Unbind first in order to avoid any duplicate handler
        // (see https://github.com/l-lin/angular-datatables/issues/87)
        $('td', row).unbind('click');
        $('td', row).bind('click', () => {
          self.someClickHandler(id);
        });
        return row;
      }

    };
 
  }

  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();
    this.dtTrigger.unsubscribe();
  }

}
