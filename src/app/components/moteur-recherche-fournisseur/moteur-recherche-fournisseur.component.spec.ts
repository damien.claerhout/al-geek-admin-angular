import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoteurRechercheFournisseurComponent } from './moteur-recherche-fournisseur.component';

describe('MoteurRechercheFournisseurComponent', () => {
  let component: MoteurRechercheFournisseurComponent;
  let fixture: ComponentFixture<MoteurRechercheFournisseurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoteurRechercheFournisseurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoteurRechercheFournisseurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
