import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { CommandeDelegateService } from 'src/app/delegate/commande-delegate.service';
import { Commande } from 'src/app/interfaces/commande';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DataTableDirective } from 'angular-datatables';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-commandes-pending',
  templateUrl: './list-commandes-pending.component.html',
  styleUrls: ['./list-commandes-pending.component.css']
})
export class ListCommandesPendingComponent implements OnInit {

  commandesATraiter: Commande[];

  @Input() id: number

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();
  
  _destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor(  private router: Router, private commandeDelegate: CommandeDelegateService ) { }

  ngOnInit(): void {
    this.getCommandesATraiter();
  }

  getCommandesATraiter(){
    this.commandeDelegate.getCommandesAtraiter().pipe(takeUntil(this._destroyed$))
                                                .subscribe((res: Commande[]) => {
                                                    this.commandesATraiter = res;
                                                    this.dtTrigger.next();
                                                }, (err) => {
                                                  console.log('-----> err', err);;
    });

    this.dtOptions = {
      language: {
          processing:     "Traitement...",
          emptyTable:     "Aucune donnée disponible dans le tableau",
          info:           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
          infoEmpty:      "Affichage de l'élément 0 à 0 sur 0 élément",
          infoFiltered:   "(filtré à partir de _MAX_ éléments au total)",
          infoPostFix:    "",
          lengthMenu:     "Afficher _MENU_ éléments",
          loadingRecords: "Chargement...",
          search:         "Rechercher :",
          zeroRecords:    "Aucun élément correspondant trouvé",
          paginate: {
              first:    "Premier",
              last:     "Dernier",
              next:     "Suivant",
              previous: "Précédent"
          },
          aria: {
              sortAscending:  ": activer pour trier la colonne par ordre croissant",
              sortDescending: ": activer pour trier la colonne par ordre décroissant"
          }
      },
      responsive: true,
      
      rowCallback: (row: Node, id: any[] | Object, index: number) => {
          const self = this;
          // Unbind first in order to avoid any duplicate handler
          // (see https://github.com/l-lin/angular-datatables/issues/87)
          $('td', row).unbind('click');
          $('td', row).bind('click', () => {
              self.someClickHandler(id);
          });
          return row;
      }
    };
  }

  someClickHandler(info: any): void {
    console.log(info[0]);
    this.router.navigate(['/afficherCommande/'+ info[0]]);
  }

  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();
  }
}
