import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCommandesPendingComponent } from './list-commandes-pending.component';

describe('ListCommandesPendingComponent', () => {
  let component: ListCommandesPendingComponent;
  let fixture: ComponentFixture<ListCommandesPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCommandesPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCommandesPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
