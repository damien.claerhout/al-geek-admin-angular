import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheFournisseurComponent } from './fiche-fournisseur.component';

describe('FicheFournisseurComponent', () => {
  let component: FicheFournisseurComponent;
  let fixture: ComponentFixture<FicheFournisseurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheFournisseurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheFournisseurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
