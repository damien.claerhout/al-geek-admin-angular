import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Fournisseur } from 'src/app/interfaces/fournisseur';
import { FournisseurDelegateService } from 'src/app/delegate/fournisseur-delegate.service';
import { Approvisionnement } from 'src/app/interfaces/approvisionnement';
import { ApprovisionnementDelegateService } from 'src/app/delegate/approvisionnement-delegate.service';
import { DataTableDirective } from 'angular-datatables';


@Component({
  selector: 'app-fiche-fournisseur',
  templateUrl: './fiche-fournisseur.component.html',
  styleUrls: ['./fiche-fournisseur.component.css']
})
export class FicheFournisseurComponent implements OnInit {

  selectedFournisseur: Fournisseur;
  fournisseur_id: string;
  _destroyed$: Subject<boolean> = new Subject<boolean>();
  approvisionnements: Approvisionnement[];

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();


  constructor(private router: Router,
              private fournisseurDelegateService :FournisseurDelegateService,
              private approvisionnementDelegateService : ApprovisionnementDelegateService,
              private route: ActivatedRoute ) {}

  updateFournisseur(): void {
    this.router.navigate(['/updateFournisseur/'+this.selectedFournisseur.id]);
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(takeUntil(this._destroyed$)).subscribe(params => {
      this.fournisseur_id = params.get('id');
      console.log(this.fournisseur_id);

      
      const promise =this.fournisseurDelegateService.rechercherParId(parseInt(this.fournisseur_id)).pipe(takeUntil(this._destroyed$)).toPromise();
      promise.then((res: Fournisseur)=>{
          console.log(res);
          this.selectedFournisseur = res;
          this.approvisionnementDelegateService.findByFournisseur(this.selectedFournisseur).pipe(takeUntil(this._destroyed$)).subscribe((res: Approvisionnement[]) =>{
              console.log(res);
              this.approvisionnements = res;
              this.dtTrigger.next(); 
        });
      }, (err) => {
        console.log('-----> err', err);
      });
    });



    this.dtOptions = {
      paging: false,
      info: false,
      searching: false,
      order: [ 1, 'desc' ],
   
      language: {
        processing:     "Traitement...",
        emptyTable:     "Aucune donnée disponible dans le tableau",
        info:           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        infoEmpty:      "Affichage de l'élément 0 à 0 sur 0 élément",
        infoFiltered:   "(filtré à partir de _MAX_ éléments au total)",
        infoPostFix:    "",
        lengthMenu:     "Afficher _MENU_ éléments",
        loadingRecords: "Chargement...",
        search:         "Rechercher :",
        zeroRecords:    "Aucun élément correspondant trouvé",
        paginate: {
            first:    "Premier",
            last:     "Dernier",
            next:     "Suivant",
            previous: "Précédent"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
      },
      responsive: true
    };
 
  }                                                            
  
  ngOnDestroy() {
    this._destroyed$.next(true);
    // Unsubscribe from the subject
    this._destroyed$.unsubscribe();

  }


}
