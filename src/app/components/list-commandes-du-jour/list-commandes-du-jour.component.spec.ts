import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCommandesDuJourComponent } from './list-commandes-du-jour.component';

describe('ListCommandesDuJourComponent', () => {
  let component: ListCommandesDuJourComponent;
  let fixture: ComponentFixture<ListCommandesDuJourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCommandesDuJourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCommandesDuJourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
