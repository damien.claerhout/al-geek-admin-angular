import { Component, OnInit, OnDestroy } from '@angular/core';
import { Produit } from 'src/app/interfaces/produit';
import { Subject } from 'rxjs';
import { ProduitDelegateService } from 'src/app/delegate/produit-delegate.service';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { TypeProduit } from 'src/app/interfaces/type-produit';

@Component({
  selector: 'app-update-fiche-produit',
  templateUrl: './update-fiche-produit.component.html',
  styleUrls: ['./update-fiche-produit.component.css']
})
export class UpdateFicheProduitComponent implements OnInit, OnDestroy{

  selectedProduit: Produit;
  produit_id: string;
  typesProduit: TypeProduit[] = []
  _destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor(private produitDelegateService : ProduitDelegateService, private route: ActivatedRoute, private router: Router) { }


  updateProduit() {
    const promise =this.produitDelegateService.modifier(this.selectedProduit).pipe(takeUntil(this._destroyed$)).toPromise();
    promise.then((res: Produit)=>{
        console.log(res);
        this.router.navigate(['/ficheProduit/'+this.selectedProduit.id]);

    }, (err) => {
      console.log('-----> err', err);
    });
   }

   onSubmit() {
    
    this.updateProduit();
  }

   onSelectedChange(value: TypeProduit) {

    console.log(value);
    this.selectedProduit.type = value;
  }


  ngOnInit(): void {

    this.produitDelegateService.getTousTypesProduits().pipe(takeUntil(this._destroyed$))
                                                      .subscribe((res: TypeProduit[]) =>{
                                                          console.log(res);
                                                          this.typesProduit = res;
    });

    this.route.paramMap.subscribe(params => {

      this.produit_id = params.get('id');
      console.log(this.produit_id);

      this.produitDelegateService.rechercherParId(parseInt(this.produit_id)).pipe(takeUntil(this._destroyed$))
                                                                            .subscribe((res: Produit) =>{
                                                                                console.log(res);
                                                                                this.selectedProduit = res;
                                                                              
                                                                            }, (err) => {
                                                                                console.log('-----> err', err);
                                                                            });
    });
  }

  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();

  }

}
