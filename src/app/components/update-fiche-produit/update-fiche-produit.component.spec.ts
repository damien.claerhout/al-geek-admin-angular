import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFicheProduitComponent } from './update-fiche-produit.component';

describe('UpdateFicheProduitComponent', () => {
  let component: UpdateFicheProduitComponent;
  let fixture: ComponentFixture<UpdateFicheProduitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFicheProduitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFicheProduitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
