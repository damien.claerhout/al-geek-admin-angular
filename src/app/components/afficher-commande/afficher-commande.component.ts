import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { LigneCommande } from 'src/app/interfaces/ligne-commande';
import { Commande } from 'src/app/interfaces/commande';
import { CommandeDelegateService } from 'src/app/delegate/commande-delegate.service';
import { ProduitDelegateService } from 'src/app/delegate/produit-delegate.service';
import { StockDelegateService } from 'src/app/delegate/stock-delegate.service';
import { takeUntil } from 'rxjs/operators';
import { LigneCommandeDelegateService } from 'src/app/delegate/ligne-commande-delegate.service';
import { Stock } from 'src/app/interfaces/stock';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-afficher-commande',
  templateUrl: './afficher-commande.component.html',
  styleUrls: ['./afficher-commande.component.css']
})
export class AfficherCommandeComponent implements OnInit, OnDestroy {

  selectedCommande: Commande;
  commande_id: string;
  stock: Stock;
  isExpediee = false;
  _destroyed$: Subject<boolean> = new Subject<boolean>();

  lastUpdate;

  @ViewChild(DataTableDirective, {static : false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  


  constructor(private router: Router,
              private commandeDelegateService: CommandeDelegateService,
              private stockDelegateService: StockDelegateService,
              private route: ActivatedRoute) { }

  traiterCommande(){
    if(this.selectedCommande.dateExpedition===null) {
			
			for (let ligne of this.selectedCommande.listLigneCommande) {

        const promise = this.stockDelegateService.getByProduit(ligne.produit).pipe(takeUntil(this._destroyed$)).toPromise();

        promise.then((res: Stock)=>{
              this.stock = res;
              this.stock.quantiteDispoPhysique = this.stock.quantiteDispoPhysique - ligne.quantite;
              
              this.stockDelegateService.modifier(this.stock).pipe(takeUntil(this._destroyed$)).subscribe((res: Stock) =>{
                  console.log(res);
                  this.stock = res; 
              });
        });

			}
			
      // mise à jour date expédition commande
      this.selectedCommande.dateExpedition = new Date();

      const promise =this.commandeDelegateService.modifier(this.selectedCommande).pipe(takeUntil(this._destroyed$)).toPromise();
      promise.then((res: Commande)=>{
          console.log(res);
          this.router.navigate(['/ficheClient/'+this.selectedCommande.client.id]);
      }, (err) => {
        console.log('-----> err', err);
      });
      
    }
  }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      this.commande_id = params.get('id');
      console.log(this.commande_id);

      
      this.commandeDelegateService.rechercherParId(parseInt(this.commande_id)).pipe(takeUntil(this._destroyed$)).subscribe((res: Commande) =>{
        console.log(res);
        this.selectedCommande = res; 
        if (this.selectedCommande.dateExpedition != null) {
          this.isExpediee = true;
        }
        this.dtTrigger.next();  
      }, (err) => {
        console.log('-----> err', err);
      });

    });

    this.dtOptions = {
      paging: false,
      info: false,
      searching: false,
      language: {
        processing:     "Traitement...",
        emptyTable:     "Aucune donnée disponible dans le tableau",
        info:           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        infoEmpty:      "Affichage de l'élément 0 à 0 sur 0 élément",
        infoFiltered:   "(filtré à partir de _MAX_ éléments au total)",
        infoPostFix:    "",
        lengthMenu:     "Afficher _MENU_ éléments",
        loadingRecords: "Chargement...",
        search:         "Rechercher :",
        zeroRecords:    "Aucun élément correspondant trouvé",
        paginate: {
            first:    "Premier",
            last:     "Dernier",
            next:     "Suivant",
            previous: "Précédent"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
      },
      responsive: true,


    };
 
  }                                                            
  
  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();
    this.dtTrigger.unsubscribe();

  }

}
