import { Approvisionnement } from './../../interfaces/approvisionnement';
import { Stock } from './../../interfaces/stock';
import { StockDelegateService } from './../../delegate/stock-delegate.service';
import { ApprovisionnementDelegateService } from './../../delegate/approvisionnement-delegate.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-afficher-approvisionnement',
  templateUrl: './afficher-approvisionnement.component.html',
  styleUrls: ['./afficher-approvisionnement.component.css']
})

export class AfficherApprovisionnementComponent implements OnInit, OnDestroy {

  approvisionnements: Approvisionnement[]= [];
  stock_id: string;
  selectedStock: Stock;
  

  @ViewChild(DataTableDirective, {static : false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  _destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor(private route: ActivatedRoute, private approvisionnementDelegateService: ApprovisionnementDelegateService, private stockDelegateService: StockDelegateService) { }

  ngOnInit(): void {

      this.route.paramMap.pipe(takeUntil(this._destroyed$)).subscribe(params => {
          this.stock_id = params.get('id');
          console.log(this.stock_id);

          
          const promise =this.stockDelegateService.rechercherParId(parseInt(this.stock_id)).pipe(takeUntil(this._destroyed$)).toPromise();
          promise.then((res: Stock)=>{
              console.log(res);
              this.selectedStock = res;
              this.approvisionnementDelegateService.findByStock(this.selectedStock).pipe(takeUntil(this._destroyed$)).subscribe((res: Approvisionnement[]) =>{
                  console.log(res);
                  this.approvisionnements = res;
                  this.dtTrigger.next(); 
            });
          }, (err) => {
            console.log('-----> err', err);
          });
      });

      this.dtOptions = {
        language: {
          processing:     "Traitement...",
          emptyTable:     "Aucune donnée disponible dans le tableau",
          info:           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
          infoEmpty:      "Affichage de l'élément 0 à 0 sur 0 élément",
          infoFiltered:   "(filtré à partir de _MAX_ éléments au total)",
          infoPostFix:    "",
          lengthMenu:     "Afficher _MENU_ éléments",
          loadingRecords: "Chargement...",
          search:         "Rechercher :",
          zeroRecords:    "Aucun élément correspondant trouvé",
          paginate: {
              first:    "Premier",
              last:     "Dernier",
              next:     "Suivant",
              previous: "Précédent"
          },
          aria: {
              sortAscending:  ": activer pour trier la colonne par ordre croissant",
              sortDescending: ": activer pour trier la colonne par ordre décroissant"
          }
        },
        responsive: true,
      };
   
  }

  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();
  }
}
