import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfficherApprovisionnementComponent } from './afficher-approvisionnement.component';

describe('AfficherApprovisionnementComponent', () => {
  let component: AfficherApprovisionnementComponent;
  let fixture: ComponentFixture<AfficherApprovisionnementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfficherApprovisionnementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfficherApprovisionnementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
