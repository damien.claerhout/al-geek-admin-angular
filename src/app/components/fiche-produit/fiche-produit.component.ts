import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Produit } from 'src/app/interfaces/produit';
import { ProduitDelegateService } from 'src/app/delegate/produit-delegate.service';

@Component({
  selector: 'app-fiche-produit',
  templateUrl: './fiche-produit.component.html',
  styleUrls: ['./fiche-produit.component.css']
})
export class FicheProduitComponent implements OnInit {

  produit: Produit;
  produit_id: string;
  _destroyed$: Subject<boolean> = new Subject<boolean>();


  constructor(private router: Router, private produitDelegateService :ProduitDelegateService, private route: ActivatedRoute ) { }

   updateFicheProduit(): void {
    
    this.router.navigate(['/updateFicheProduit/'+this.produit.id]);
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(takeUntil(this._destroyed$)).subscribe(params => {
      this.produit_id = params.get('id');
      console.log(this.produit_id);

      
      this.produitDelegateService.rechercherParId(parseInt(this.produit_id)).pipe(takeUntil(this._destroyed$)).subscribe((res: Produit) =>{
        console.log(res);
        this.produit = res;
  
      }, (err) => {
        console.log('-----> err', err);
      });
    });
 
  }                                                            
  
  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();
  }
}
