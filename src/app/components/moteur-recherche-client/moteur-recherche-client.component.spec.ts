import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoteurRechercheClientComponent } from './moteur-recherche-client.component';

describe('MoteurRechercheClientComponent', () => {
  let component: MoteurRechercheClientComponent;
  let fixture: ComponentFixture<MoteurRechercheClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoteurRechercheClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoteurRechercheClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
