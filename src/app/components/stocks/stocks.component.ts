import { ApprovisionnementDelegateService } from './../../delegate/approvisionnement-delegate.service';
import { Stock } from 'src/app/interfaces/stock';
import { TypeProduit } from './../../interfaces/type-produit';
import { Produit } from 'src/app/interfaces/produit';
import { ProduitDelegateService } from './../../delegate/produit-delegate.service';
import { StockDelegateService } from './../../delegate/stock-delegate.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild, OnDestroy, Input } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Approvisionnement } from 'src/app/interfaces/approvisionnement';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit, OnDestroy {

  selectedStock: Stock;
  stocks: Stock[] = [];
  stock_id: string;
  typesProduit: TypeProduit[] = [];
  approvisionnements: Approvisionnement[];

  @ViewChild(DataTableDirective, {static : false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  
  _destroyed$: Subject<boolean> = new Subject<boolean>();
  

  constructor(private router: Router, private route: ActivatedRoute, private stockDelegateService: StockDelegateService, private produitDelegateService: ProduitDelegateService, private approvisionnementDelegateService: ApprovisionnementDelegateService) { }

  afficherApprovisionnement(): void { 
    this.router.navigate(['/afficherApprovisionnement/'+this.selectedStock.id]);
  }

  someClickHandler(info: any): void {
    console.log(info[0]);
    this.router.navigate(['/afficherApprovisionnement/'+ info[0]]);
  }
  
  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      this.stock_id = params.get('id');
      console.log(this.stock_id);
    
    this.produitDelegateService.getTousTypesProduits().pipe(takeUntil(this._destroyed$))
                                                      .subscribe((res: TypeProduit[]) =>{
                                                          console.log(res);
                                                          this.typesProduit = res;
                                                      }, (err) => {
                                                          console.log(' *** erreur ***', err);                                                   
    });

    this.stockDelegateService.getAll().pipe(takeUntil(this._destroyed$))
                                          .subscribe((res: Stock[]) =>{
                                              console.log(res);
                                              this.stocks = res;
                                              this.dtTrigger.next();
                                          }, (err) => {
                                              console.log('-----> err', err);
    });
  });

    this.dtOptions = {
      language: {
        processing:     "Traitement...",
        emptyTable:     "Aucune donnée disponible dans le tableau",
        info:           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        infoEmpty:      "Affichage de l'élément 0 à 0 sur 0 élément",
        infoFiltered:   "(filtré à partir de _MAX_ éléments au total)",
        infoPostFix:    "",
        lengthMenu:     "Afficher _MENU_ éléments",
        loadingRecords: "Chargement...",
        search:         "Rechercher :",
        zeroRecords:    "Aucun élément correspondant trouvé",
        paginate: {
            first:    "Premier",
            last:     "Dernier",
            next:     "Suivant",
            previous: "Précédent"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
      },
      pageLength: 5,
      responsive: true,

      rowCallback: (row: Node, id: any[] | Object, index: number) => {
        const self = this;
        // Unbind first in order to avoid any duplicate handler
        // (see https://github.com/l-lin/angular-datatables/issues/87)
        $('td', row).unbind('click');
        $('td', row).bind('click', () => {
          self.someClickHandler(id);
        });
        return row;
      }

    };
 
  }

  updateCatalogue(selectedTypeProduitId: number) {
    if (selectedTypeProduitId == 666) {
      this.rechargerTousLesProduits();
    } else {

      this.stockDelegateService.getByTypeProduit(selectedTypeProduitId).pipe(takeUntil(this._destroyed$))
                                                                      .subscribe((res: Stock[]) =>{
                                                                          console.log(res);
                                                                          console.log(selectedTypeProduitId);
                                                                          this.stocks = res;
                                                                          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                                                                            dtInstance.destroy();
                                                                            this.dtTrigger.next();
                                                                          },
                                                                          (error) => {
                                                                            console.log('erreur', error);
                                                                          });   
      });
    }
  }


  rechargerTousLesProduits() {
    this.stockDelegateService.getAll().pipe(takeUntil(this._destroyed$))
                                      .subscribe((res: Stock[]) =>{
                                        console.log(res);
                                        this.stocks = res;
                                        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                                          dtInstance.destroy();
                                          this.dtTrigger.next();
                                        });     
    });   
  }

  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();
    this.dtTrigger.unsubscribe();
  }

}
