import { Component, OnInit } from '@angular/core';
import { CommandeDelegateService } from 'src/app/delegate/commande-delegate.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Commande } from 'src/app/interfaces/commande';
import { StockDelegateService } from 'src/app/delegate/stock-delegate.service';
import { Stock } from 'src/app/interfaces/stock';

declare var compteurCommandesATraiter;
declare var compteurCommandeDuJour;
declare var compteurEtatStock;
declare var compteurChiffreAff;

@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.component.html',
  styleUrls: ['./home-admin.component.css']
})


export class HomeAdminComponent implements OnInit {

  
  nbCommandeATraiter: number;
  nbProduitRuptureStock: number;
  commandeDuJour: Commande[] = [];
  chiffreAffairesDuJour: number = 0;

  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private commandeDelegate: CommandeDelegateService,
    private stockDelegate: StockDelegateService) { }

  ngOnInit(): void {
    this.getNbCommandeATraiter();
    this.getNbProduitRuptureStock();
    this.getChiffreAffDuJour();
  }


  getNbCommandeATraiter() {
    this.commandeDelegate.getCommandesAtraiter().
      pipe(takeUntil(this.destroy$))
      .subscribe((res: Commande[]) => {
        this.nbCommandeATraiter = res.length;
        this.callcompteurCommandesATraiter(this.nbCommandeATraiter);
      }, (err) => {
        console.log('-----> err', err);;
      });
  }


  getNbProduitRuptureStock() {
    this.stockDelegate.getByQuantiteDispo(10).
      pipe(takeUntil(this.destroy$))
      .subscribe((res: Stock[]) => {
        this.nbProduitRuptureStock = res.length;
        compteurEtatStock(this.nbProduitRuptureStock);
      }, (err) => {
        console.log('-----> err', err);;
      });
  }


  getChiffreAffDuJour() {
    this.commandeDelegate.getCommandesDuJour(new Date()).
      pipe(takeUntil(this.destroy$))
      .subscribe((res: Commande[]) => {
        this.commandeDuJour = res;
        for (let cmd of this.commandeDuJour) {
          if (this.commandeDuJour != null) {
            this.chiffreAffairesDuJour += cmd.prix;
          }
        }
        this.callcompteurChiffreAff(this.chiffreAffairesDuJour);
        this.callcompteurCommandeDuJour(this.commandeDuJour.length)
      }, (err) => {
        console.log('-----> err', err);
      });
  }


  callcompteurCommandesATraiter(nbCommande) {
    compteurCommandesATraiter(nbCommande);
  }

  callcompteurCommandeDuJour(nbCommandeJour) {
    compteurCommandeDuJour(nbCommandeJour);
  }

  callcompteurChiffreAff(chiffreAff){
    compteurChiffreAff(chiffreAff);
  }

  callcompteurEtatStock(etatStock) {
    compteurEtatStock(etatStock);
  }
  

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
