import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Router, ActivatedRoute } from '@angular/router';
import { StockDelegateService } from 'src/app/delegate/stock-delegate.service';
import { ProduitDelegateService } from 'src/app/delegate/produit-delegate.service';
import { Stock } from 'src/app/interfaces/stock';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-stocks-rupture',
  templateUrl: './stocks-rupture.component.html',
  styleUrls: ['./stocks-rupture.component.css']
})
export class StocksRuptureComponent implements OnInit {

  produitRuptureStock: Stock[] = [];
  selectedStock: Stock;
  stock_id: string;

  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  _destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor(private router: Router,
    private route: ActivatedRoute,
    private stockDelegate: StockDelegateService,
    private produitDelegateService: ProduitDelegateService) { }


  afficherApprovisionnement(): void {
    this.router.navigate(['/afficherApprovisionnement/' + this.selectedStock.id]);
  }

  someClickHandler(info: any): void {
    console.log(info[0]);
    this.router.navigate(['/afficherApprovisionnement/' + info[0]]);
  }


  ngOnInit(): void {

    this.route.paramMap.pipe(takeUntil(this._destroyed$)).subscribe(params => {
      this.stock_id = params.get('id');
    this.getProduitRuptureStock();

  });


    this.dtOptions = {
      order: [5, 'asc'],
      language: {

        processing: "Traitement...",
        emptyTable: "Aucune donnée disponible dans le tableau",
        info: "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        infoEmpty: "Affichage de l'élément 0 à 0 sur 0 élément",
        infoFiltered: "(filtré à partir de _MAX_ éléments au total)",
        infoPostFix: "",
        lengthMenu: "Afficher _MENU_ éléments",
        loadingRecords: "Chargement...",
        search: "Rechercher :",
        zeroRecords: "Aucun élément correspondant trouvé",
        paginate: {
          first: "Premier",
          last: "Dernier",
          next: "Suivant",
          previous: "Précédent"
        },
        aria: {
          sortAscending: ": activer pour trier la colonne par ordre croissant",
          sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
      },
      responsive: true,

      rowCallback: (row: Node, id: any[] | Object, index: number) => {
        const self = this;
        // Unbind first in order to avoid any duplicate handler
        // (see https://github.com/l-lin/angular-datatables/issues/87)
        $('td', row).unbind('click');
        $('td', row).bind('click', () => {
          self.someClickHandler(id);
        });
        return row;
      }

    };
  }



  getProduitRuptureStock() {
    this.stockDelegate.getByQuantiteDispo(10).pipe(takeUntil(this._destroyed$))
                                              .subscribe((res: Stock[]) => {
                                                this.produitRuptureStock = res;
                                                this.dtTrigger.next();
                                              }, (err) => {
                                                console.log('-----> err', err);;
                                              });
  }



  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.unsubscribe();
    this.dtTrigger.unsubscribe();
  }

}
