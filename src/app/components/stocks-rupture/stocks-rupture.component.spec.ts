import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StocksRuptureComponent } from './stocks-rupture.component';

describe('StocksRuptureComponent', () => {
  let component: StocksRuptureComponent;
  let fixture: ComponentFixture<StocksRuptureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StocksRuptureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksRuptureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
