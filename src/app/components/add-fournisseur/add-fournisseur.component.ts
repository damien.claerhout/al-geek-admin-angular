import { Component, OnInit, OnDestroy } from '@angular/core';
import { Fournisseur } from 'src/app/interfaces/fournisseur';
import { FournisseurDelegateService } from 'src/app/delegate/fournisseur-delegate.service';
import { Router } from '@angular/router';
import { VilleDelegateService } from 'src/app/delegate/ville-delegate.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Ville } from 'src/app/interfaces/ville';

@Component({
  selector: 'app-add-fournisseur',
  templateUrl: './add-fournisseur.component.html',
  styleUrls: ['./add-fournisseur.component.css']
})
export class AddFournisseurComponent implements OnInit, OnDestroy{

  fournisseur : Fournisseur = new Fournisseur();
  submitted = false;
  _destroyed$: Subject<boolean> = new Subject<boolean>();
  villes : Ville[] = [];

  constructor(private fournisseurDelegateService: FournisseurDelegateService, private villeDelegateService: VilleDelegateService, private router: Router) { }

  ajouter() {

    const promise =this.fournisseurDelegateService.modifier(this.fournisseur).pipe(takeUntil(this._destroyed$)).toPromise();
    promise.then((res: Fournisseur)=>{
        console.log(res);
        this.router.navigate(['/moteurFournisseur']);
    }, (err) => {
      console.log('-----> err', err);
    });
  }

  onSubmit() {
    this.submitted = true;
    this.ajouter();
  }
  
  ngOnInit(): void {


    this.villeDelegateService.getAll().pipe(takeUntil(this._destroyed$))
    .subscribe((res: Ville[]) =>{
        this.villes = res;
        console.log(res);
    });
  }
    
  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();

  }

}
