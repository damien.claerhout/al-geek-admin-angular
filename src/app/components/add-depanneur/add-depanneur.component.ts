import { Component, OnInit, OnDestroy } from '@angular/core';

import { Router } from '@angular/router';
import { DepannageDelegateService } from 'src/app/delegate/depannage-delegate.service';
import { Depanneur } from 'src/app/interfaces/depanneur';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-add-depanneur',
  templateUrl: './add-depanneur.component.html',
  styleUrls: ['./add-depanneur.component.css']
})
export class AddDepanneurComponent implements OnInit, OnDestroy {

  newDepanneur : Depanneur = new Depanneur();
  submitted = false;
  _destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor(private depannageDelegateService: DepannageDelegateService, private router: Router) { }

  ngOnInit(): void {}

  ajouter() {

    const promise =this.depannageDelegateService.modifier(this.newDepanneur).pipe(takeUntil(this._destroyed$)).toPromise();
    promise.then((res: Depanneur)=>{
        console.log(res);
        this.router.navigate(['/moteurDepanneur']);
    }, (err) => {
      console.log('-----> err', err);
    });

  }

  onSubmit() {
    this.submitted = true;
    this.ajouter();
  }

  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();

  }
}
