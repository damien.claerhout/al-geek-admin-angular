import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Router } from '@angular/router';
import { Produit } from 'src/app/interfaces/produit';
import { TypeProduit } from 'src/app/interfaces/type-produit';
import { CatalogueDelegateService } from 'src/app/delegate/catalogue-delegate.service';
import { ProduitDelegateService } from 'src/app/delegate/produit-delegate.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit,  OnDestroy {

  produits: Produit[] = [];
  typesProduit: TypeProduit[] = [];

  @ViewChild(DataTableDirective, {static : false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  
  _destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor(private router: Router, private catalogueDelegateService: CatalogueDelegateService, private produitDelegateService: ProduitDelegateService) { }

  someClickHandler(info: any): void {
    console.log(info[0]);
    this.router.navigate(['/ficheProduit/'+ info[0]]);
  }


  ngOnInit(): void {

    this.produitDelegateService.getTousTypesProduits().pipe(takeUntil(this._destroyed$))
                                                      .subscribe((res: TypeProduit[]) =>{
                                                          console.log(res);
                                                          this.typesProduit = res;
                                                      }, (err) => {
                                                          console.log(' *** erreur ***', err);                                                   
    });

    this.catalogueDelegateService.getAll().pipe(takeUntil(this._destroyed$))
                                          .subscribe((res: Produit[]) =>{
                                              console.log(res);
                                              this.produits = res;
                                              this.dtTrigger.next();
                                          }, (err) => {
                                              console.log('-----> err', err);
    });



    this.dtOptions = {
      language: {
        processing:     "Traitement...",
        emptyTable:     "Aucune donnée disponible dans le tableau",
        info:           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        infoEmpty:      "Affichage de l'élément 0 à 0 sur 0 élément",
        infoFiltered:   "(filtré à partir de _MAX_ éléments au total)",
        infoPostFix:    "",
        lengthMenu:     "Afficher _MENU_ éléments",
        loadingRecords: "Chargement...",
        search:         "Rechercher :",
        zeroRecords:    "Aucun élément correspondant trouvé",
        paginate: {
            first:    "Premier",
            last:     "Dernier",
            next:     "Suivant",
            previous: "Précédent"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
      },
      pageLength: 5,
      responsive: true,

      rowCallback: (row: Node, id: any[] | Object, index: number) => {
        const self = this;
        // Unbind first in order to avoid any duplicate handler
        // (see https://github.com/l-lin/angular-datatables/issues/87)
        $('td', row).unbind('click');
        $('td', row).bind('click', () => {
          self.someClickHandler(id);
        });
        return row;
      }
    };
  }

  updateCatalogue(selectedTypeProduitId: number) {
    if (selectedTypeProduitId == 666) {
      this.rechargerTousLesProduits();
    } else {

      this.produitDelegateService.getProduitsParType(selectedTypeProduitId, true).pipe(takeUntil(this._destroyed$))
      .subscribe((res: Produit[]) =>{
        console.log(res);
        console.log(selectedTypeProduitId);
        this.produits = res;
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
        },
        (error) => {
          console.log('erreur', error);
        });   
      });
    }
  }

  rechargerTousLesProduits() {
    this.catalogueDelegateService.getAll().pipe(takeUntil(this._destroyed$))
                                          .subscribe((res: Produit[]) =>{
                                            console.log(res);
                                            this.produits = res;
                                            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                                              dtInstance.destroy();
                                              this.dtTrigger.next();
                                            });     
                                          });   
  }


  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();
    this.dtTrigger.unsubscribe();
  }
}
