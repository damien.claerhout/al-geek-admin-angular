import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Depanneur } from 'src/app/interfaces/depanneur';
import { DepannageDelegateService } from 'src/app/delegate/depannage-delegate.service';

@Component({
  selector: 'app-moteur-recherche-depanneur',
  templateUrl: './moteur-recherche-depanneur.component.html',
  styleUrls: ['./moteur-recherche-depanneur.component.css']
})
export class MoteurRechercheDepanneurComponent implements OnInit, OnDestroy {

  depanneurs: Depanneur[] = [];

  @Input() id: number

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();

  _destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor( private router: Router, private depannageService: DepannageDelegateService) { }

  someClickHandler(info: any): void {

    this.router.navigate(['/ficheDepanneur/'+ info[0]]);
  }

  ngOnInit(): void {
    this.depannageService.getAll().pipe(takeUntil(this._destroyed$))
                                  .subscribe((res: Depanneur[]) => {
                                      this.depanneurs = res;
                                      this.dtTrigger.next();
                                      this.depannageService.depanneurs = this.depanneurs;
                                  }, (err) => {
                                      console.log('Impossible de charger la liste des dépanneurs', err);
    });
    
    this.dtOptions = {
      order: [ 1, 'asc' ],
      language: {
          processing:     "Traitement...",
          emptyTable:     "Aucune donnée disponible dans le tableau",
          info:           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
          infoEmpty:      "Affichage de l'élément 0 à 0 sur 0 élément",
          infoFiltered:   "(filtré à partir de _MAX_ éléments au total)",
          infoPostFix:    "",
          lengthMenu:     "Afficher _MENU_ éléments",
          loadingRecords: "Chargement...",
          search:         "Rechercher :",
          zeroRecords:    "Aucun élément correspondant trouvé",
          paginate: {
              first:    "Premier",
              last:     "Dernier",
              next:     "Suivant",
              previous: "Précédent"
          },
          aria: {
              sortAscending:  ": activer pour trier la colonne par ordre croissant",
              sortDescending: ": activer pour trier la colonne par ordre décroissant"
          }
      },
      responsive: true,
      
      rowCallback: (row: Node, id: any[] | Object, index: number) => {
          const self = this;
          // Unbind first in order to avoid any duplicate handler
          // (see https://github.com/l-lin/angular-datatables/issues/87)
          $('td', row).unbind('click');
          $('td', row).bind('click', () => {
              self.someClickHandler(id);
          });
          return row;
      }
      
    };
  }

  ngOnDestroy(): void {
    this._destroyed$.next(true);
    this._destroyed$.complete();
  }


}
