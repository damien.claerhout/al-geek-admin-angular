import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject} from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Client } from 'src/app/interfaces/client';
import { Commande } from 'src/app/interfaces/commande';
import { ClientDelegateService } from 'src/app/delegate/client-delegate.service';
import { CommandeDelegateService } from 'src/app/delegate/commande-delegate.service';



@Component({
  selector: 'app-fiche-client',
  templateUrl: './fiche-client.component.html',
  styleUrls: ['./fiche-client.component.css'],
})

export class FicheClientComponent implements OnInit, OnDestroy  {

  selectedClient: Client;
  client_id: string;
  commandes: Commande[] = [];
  _destroyed$: Subject<boolean> = new Subject<boolean>();

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();


  constructor(private router: Router, private clientDelegateService :ClientDelegateService, private commandeDelegateService: CommandeDelegateService, private route: ActivatedRoute ) {}

  updateFicheClient(): void {
    
    this.router.navigate(['/updateFicheClient/'+this.selectedClient.id]);
  }

  someClickHandler(info: any): void {
    this.router.navigate(['/afficherCommande/'+ info[0]]);
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(takeUntil(this._destroyed$)).subscribe(params => {
      this.client_id = params.get('id');

      this.clientDelegateService.rechercherParId(parseInt(this.client_id)).pipe(takeUntil(this._destroyed$)).subscribe((res: Client) =>{
        console.log(res);
        this.selectedClient = res;  
      }, (err) => {
        console.log('-----> err', err);
      });

      this.commandeDelegateService.getCommandesPourClient(parseInt(this.client_id)).pipe(takeUntil(this._destroyed$)).subscribe((res: Commande[]) =>{
        console.log(res);
        this.commandes = res;
        this.dtTrigger.next();  
      }, (err) => {
        console.log('-----> err', err);
      });


    });

    this.dtOptions = {
      paging: false,
      info: false,
      searching: false,
      order: [ 1, 'desc' ],
   
      language: {
        processing:     "Traitement...",
        emptyTable:     "Aucune donnée disponible dans le tableau",
        info:           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        infoEmpty:      "Affichage de l'élément 0 à 0 sur 0 élément",
        infoFiltered:   "(filtré à partir de _MAX_ éléments au total)",
        infoPostFix:    "",
        lengthMenu:     "Afficher _MENU_ éléments",
        loadingRecords: "Chargement...",
        search:         "Rechercher :",
        zeroRecords:    "Aucun élément correspondant trouvé",
        paginate: {
            first:    "Premier",
            last:     "Dernier",
            next:     "Suivant",
            previous: "Précédent"
        },
        aria: {
            sortAscending:  ": activer pour trier la colonne par ordre croissant",
            sortDescending: ": activer pour trier la colonne par ordre décroissant"
        }
      },
      responsive: true,

      rowCallback: (row: Node, id: any[] | Object, index: number) => {
        const self = this;
        $('td', row).unbind('click');
        $('td', row).bind('click', () => {
          self.someClickHandler(id);
        });
        return row;
      }
    };
  }                                                            
  
  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();
    this.dtTrigger.unsubscribe();
  }
}
