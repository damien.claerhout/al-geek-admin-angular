import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Depanneur } from 'src/app/interfaces/depanneur';
import { DepannageDelegateService } from 'src/app/delegate/depannage-delegate.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-update-fiche-depanneur',
  templateUrl: './update-fiche-depanneur.component.html',
  styleUrls: ['./update-fiche-depanneur.component.css']
})
export class UpdateFicheDepanneurComponent implements OnInit, OnDestroy {

  selectedDepanneur: Depanneur;
  depanneur_id: string;
  _destroyed$: Subject<boolean> = new Subject<boolean>();
  submitted = false;

  constructor( private depannageDelegateService: DepannageDelegateService,
               private route: ActivatedRoute, 
               private router: Router) { }

  updateDepanneur() {
    
    const promise =this.depannageDelegateService.modifier(this.selectedDepanneur).pipe(takeUntil(this._destroyed$)).toPromise();
    promise.then((res: Depanneur)=>{
        console.log(res);
        this.router.navigate(['/moteurDepanneur']);
    }, (err) => {
      console.log('-----> err', err);
    });

  }

  onSubmit() {
    this.submitted = true;
    this.updateDepanneur();
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.selectedDepanneur = this.depannageDelegateService.getDepanneurById(+id);
  }

  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.unsubscribe();

  }
}
