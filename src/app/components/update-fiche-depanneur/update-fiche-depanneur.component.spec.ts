import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFicheDepanneurComponent } from './update-fiche-depanneur.component';

describe('UpdateFicheDepanneurComponent', () => {
  let component: UpdateFicheDepanneurComponent;
  let fixture: ComponentFixture<UpdateFicheDepanneurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFicheDepanneurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFicheDepanneurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
