import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-kibana',
  templateUrl: './kibana.component.html',
  styleUrls: ['./kibana.component.css']
})
export class KibanaComponent implements OnInit {

  destroy$: Subject<boolean> = new Subject<boolean>();
  
  constructor() { }

  ngOnInit(): void {
  }

    
  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();

  }
}
