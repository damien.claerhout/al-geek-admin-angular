function compteurCommandesATraiter(nbCommande) {
    var n = nbCommande;
    var cpt = 0;
    var duree = 0.1;
    var delta = Math.ceil((duree * 1000));
    var node = document.getElementById('compteur');

    function countdown() {
        node.innerHTML = ++cpt;

        if (cpt < n) {
            setTimeout(countdown, delta);
        }
    }

    setTimeout(countdown, delta);
}


function compteurCommandeDuJour(nbCommandeJour) {
    var n1 = nbCommandeJour;
    var cpt1 = 0;
    var duree1 = 0.1;
    var delta1 = Math.ceil((duree1 * 1000));
    var node1 = document.getElementById('cmdDuJour');

    function countdown1() {
        node1.innerHTML = ++cpt1;

        if (cpt1 < n1) {
            setTimeout(countdown1, delta1);
        }
    }

    setTimeout(countdown1, delta1);
}


function compteurEtatStock(etatStock) {
    var n2 = etatStock;
    var duree2 = 0.1;
    var cpt2 = 0;
    var delta2 = Math.ceil((duree2 * 1000));
    var node2 = document.getElementById('stockRupture');

    function countdown2() {
        node2.innerHTML = ++cpt2;

        if (cpt2 < n2) {
            setTimeout(countdown2, delta2);
        }
    }

    setTimeout(countdown2, delta2);
}


function compteurChiffreAff(chiffre) {
    var n3 = chiffre;
    var cpt3 = 0;
    var duree3 = 5;
    var delta3 = Math.ceil((duree3 * 1000) / n3);
    var node3 = document.getElementById('montant');

    function countdown3() {
        node3.innerHTML = ++cpt3 * 25 + " €";

        if (cpt3 * 25 < n3) {
            setTimeout(countdown3, delta3);
        }
    }

    setTimeout(countdown3, delta3);
}
